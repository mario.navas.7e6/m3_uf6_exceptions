fun divideixoCero(num1:Int, num2:Int): Int{
    return try{
        num1/num2
    }catch(Exception:ArithmeticException){
        0
    }
}
fun main(){
    println(divideixoCero(2,0))
}