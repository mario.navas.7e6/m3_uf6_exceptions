fun main() {
    val lista: MutableList<String> = mutableListOf("34", "monkey", "fer", "ginas")
    println("Lista original: $lista")

    try {
        lista.add("6")
        lista.add(2, "a")
        println(lista)
    } catch (ex: UnsupportedOperationException) {
        println("No se puede agregar elementos a la lista inmutable")
    }

    try {
        lista.remove("en")
        lista.removeAt(3)
        println(lista)
    } catch (ex: NoSuchElementException) {
        println("No se puede eliminar elementos que no están en la lista")
    } catch (ex: UnsupportedOperationException) {
        println("No se puede eliminar elementos de la lista inmutable")
    }
}