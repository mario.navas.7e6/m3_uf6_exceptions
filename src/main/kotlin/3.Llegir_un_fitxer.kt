import java.io.File
import java.io.IOException
import java.nio.file.Path
import kotlin.io.path.readLines

fun llegirFitxer(fitxer: File){
    try{
        var file = fitxer.readLines()
        println(file)
    }catch(Exception: IOException){
        println("S'ha produït un error d'entrada/sortida:\n$fitxer (El sistema no puede encontrar el archivo especificado)")
    }
}

fun main(){
    val file = File("dasjfk.txt")
    llegirFitxer(file)
}